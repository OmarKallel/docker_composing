﻿using Docker_API.Data;
using Docker_API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docker_API.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly AppDbContext appDbContext;

        public EmployeeRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public async Task<IEnumerable<Employee>> GetEmployeesAsync()
        {
            return await appDbContext.Employees.ToListAsync();
        }

        public async Task<Employee> GetEmployeeAsync(int employeeId)
        {
            return await appDbContext.Employees
                .FirstOrDefaultAsync(e => e.EmployeeId == employeeId);
        }

        public async Task<Employee> GetEmployeeByEmailAsync(string email)
        {
            return await appDbContext.Employees
                .FirstOrDefaultAsync(e => e.Email == email);
        }

        public async Task<Employee> AddEmployeeAsync(Docker_API.Models.Employee employee)
        {
            var result = await appDbContext.Employees.AddAsync(employee);
            await appDbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Employee> UpdateEmployeeAsync(Employee employee)
        {
            var result = await appDbContext.Employees
                .FirstOrDefaultAsync(e => e.EmployeeId == employee.EmployeeId);

            if (result != null)
            {
                result.FirstName = employee.FirstName;
                result.LastName = employee.LastName;
                result.Email = employee.Email;
                result.DateOfBirth = employee.DateOfBirth;
                //result.Gender = employee.Gender;
                result.DepartmentId = employee.DepartmentId;
                result.PhotoPath = employee.PhotoPath;

                await appDbContext.SaveChangesAsync();

                return result;
            }

            return null;
        }

        public async Task<bool> DeleteEmployeeAsync(int employeeId)
        {
            var result = await appDbContext.Employees
                .FirstOrDefaultAsync(e => e.EmployeeId == employeeId);
            if (result != null)
            {
                appDbContext.Employees.Remove(result);
                await appDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        //public async Task<IEnumerable<Employee>> Search(string name, Gender? gender)
        //{
        //    IQueryable<Employee> query = appDbContext.Employees;

        //    if (!string.IsNullOrEmpty(name))
        //    {
        //        query = query.Where(e => e.FirstName.Contains(name)
        //                    || e.LastName.Contains(name));
        //    }

        //    if (gender != null)
        //    {
        //        query = query.Where(e => e.Gender == gender);
        //    }

        //    return await query.ToListAsync();
        //}
    }
}
